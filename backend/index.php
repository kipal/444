<?

require __DIR__ . '/vendor/autoload.php';

use kipal\fw\ApplicationRunner;
use kipal\fw\APIRequest;
use kipal\fw\InputStreamHandler;
use ErrorReporting\FakeErrorsApplication;

$ar = new ApplicationRunner();
$ar->setErrorHandler("ErrorReporting\\FakeErrorsApplication::errorAPIResponse")
->execute(
	require "./config.php",
	"ErrorReporting\\FakeErrorsApplication",
	APIRequest::createFromInput(new InputStreamHandler("php://input"))
);
