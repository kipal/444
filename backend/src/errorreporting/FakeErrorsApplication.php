<?

namespace ErrorReporting;

use kipal\fw\APIApplication;
use kipal\fw\Input;
use EmailHandling\Email;

class FakeException extends \Exception
{
}

class FakeErrorsApplication extends APIApplication
{
	protected static $instance = null;

	protected static $req = null;

	public function run(Input $i)
	{
		static::$req = $i;

		if ($this->getConfig("fakeError")) {

			throw new FakeException();
		}

		return parent::run($i);
	}

	public static function errorAPIResponse(\Exception $ex)
	{
		$msg = 'Exception:' . print_r($ex, true) .
			'\n$_SERVER:' . print_r($_SERVER, true) .
			'\nRequest:' . print_r(static::$req, true);

		static::syslog($msg);
		static::sendEmail($msg);
		parent::errorAPIResponse($ex);
	}

	private static function syslog($msg)
	{
		$sl = static::getInstance()->getComponent("syslog");
		$sl->log(LOG_ERR, $msg);
	}

	private static function sendEmail($msg)
	{
		$errorMailConfig = static::getInstance()->getConfig("errorMail");
		$email = new Email();
		$email->setFrom($errorMailConfig["from"]["address"], $errorMailConfig["from"]["name"])
			->setSubject($errorMailConfig["subject"])
			->setBody($msg);

		foreach ($errorMailConfig["recepients"] as $recepient) {
			$email->addRecepient($recepient["address"], $recepient["name"]);
		}

		$emailClient = static::getInstance()->getComponent("emailClient");
		$emailClient->send($email);
	}
}
