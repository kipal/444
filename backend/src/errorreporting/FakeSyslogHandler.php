<?
namespace ErrorReporting;

class FakeSyslogHandler extends SyslogHandler
{
	public $isOpened = false;

	protected static $logHasCalled = 0;

	public static $isClosed = false;

	public function open(string $ident, int $option, int $facility)
	{
		$this->isOpened = true;
	}

	public function log(int $priority, string $msg)
	{
		static::$logHasCalled++;
	}

	public function close()
	{
		static::$isClosed = true;
	}

	public function getNumberOfLogs() : int
	{

		return static::$logHasCalled;
	}
}
