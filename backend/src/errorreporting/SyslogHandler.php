<?

namespace ErrorReporting;

use kipal\fw\Component;

class SyslogHandler extends Component
{
	public function init()
	{
		$this->open(
			$this->getIdent(),
			$this->getOption(),
			$this->getFacility()
		);
	}

	public function __destruct()
	{
		$this->close();
	}

	public function open(string $ident, int $option, int $facility)
	{
		openlog($ident, $option, $facility);
	}

	public function log(int $priority, string $msg)
	{
		syslog($priority, $msg);
	}

	public function close()
	{
		closelog();
	}
}
