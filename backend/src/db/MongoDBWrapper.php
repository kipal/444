<?

namespace DB;

use kipal\fw\Component;

class MongoDBWrapper extends Component
{
	private $driver = null;

	public function setMembers(array $m)
	{
		parent::setMembers($m);

		$this->driver = new \MongoDB\Driver\Manager($this->getAddress());
	}

	public function getDocumentById(string $collection, $id)
	{
		$query = new \MongoDB\Driver\Query(["_id" => $id]);
		$cursor = $this->driver->executeQuery($collection, $query);

		return $cursor->toArray()[0];
	}

	public function getDocumentsByFilterAndOptions(string $collection, array $filters, array $options) : array
	{
		$query = new \MongoDB\Driver\Query($filters, $options);

		return $this->driver->executeQuery($collection, $query)->toArray();
	}

	public function insertOneDocument(string $collection, $doc)
	{
		$bulk = new \MongoDB\Driver\BulkWrite();
		$id = $bulk->insert($doc);

		if (empty($id)) {
			$id = $doc["_id"];
		}

		$this->driver->executeBulkWrite($collection, $bulk);

		return $id;
	}
}
