<?

namespace BlogHandling;

use kipal\fw\APIApplication;

class Blog implements \JsonSerializable
{
	CONST KEY_COLLECTION = "db.blog";

	private static $mongoWrapper = null;

	private $id = null;

	private $title = "";

	private $author = "";

	private $date = null;

	public function __construct($id, string $title, string $author, int $date)
	{
		$this->setId($id);
		$this->setTitle($title);
		$this->setAuthor($author);
		$this->setDate($date);
	}

	public function setId($id) : Blog
	{
		$this->id = $id;

		return $this;
	}

	public function getId()
	{

		return $this->id;
	}

	public function setTitle(string $title) : Blog
	{
		$this->title = $title;

		return $this;
	}

	public function getTitle() : string
	{

		return $this->title;
	}

	public function setAuthor(string $author) : Blog
	{
		$this->author = $author;

		return $this;
	}

	public function getAuthor() : string
	{

		return $this->author;
	}

	public function setDate(int $date) : Blog
	{
		$this->date = new \DateTime("@" . $date);

		return $this;
	}

	public function getDate() : \DateTime
	{
		if (null === $this->date) {
			$this->date = new \DateTime();
		}

		return $this->date;
	}

	public static function getMongo() : \DB\MongoDBWrapper
	{
		if (empty(static::$mongoWrapper)) {
			static::$mongoWrapper = APIApplication::getInstance()->getComponent("mongo");
		}

		return static::$mongoWrapper;
	}

	public static function getById($id) : Blog
	{

		return static::deserialize(static::getMongo()->getDocumentById(static::KEY_COLLECTION, $id));
	}

	public function deserialize(\stdClass $obj) : Blog
	{

		return new Blog($obj->_id, $obj->title, $obj->author, $obj->date);

	}

	public function getAllByDateDesc() : array
	{
		$docs = static::getMongo()->getDocumentsByFilterAndOptions(
			static::KEY_COLLECTION,
			[],
			["sort" => ["date" => -1]]
		);

		$results = [];

		foreach ($docs as $d) {
			$results[] = static::deserialize($d);
		}

		return $results;
	}

	public function jsonSerialize()
	{
		return [
			"id" => $this->id,
			"title" => $this->title,
			"author" => $this->author,
			"date" => $this->date->format("D M d Y H:i:s O")
		];
	}

	public function mongoSerialize()
	{
		$result = [
			"title" => $this->title,
			"author" => $this->author,
			"date"=> $this->date->getTimeStamp()
		];

		if (0 !== $this->id) {
			$result["_id"] = $this->id;
		}

		return $result;
	}

	public function save()
	{
		if (0 === $this->id) {
			$id = static::getMongo()->insertOneDocument(static::KEY_COLLECTION, $this->mongoSerialize());
			$this->id = $id;

			return $id;
		}
		//TODO update
	}
}
