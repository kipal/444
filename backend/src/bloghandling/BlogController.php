<?

namespace BlogHandling;

use kipal\fw\APIResponse;
use kipal\fw\CLIResponse;

class BlogController
{
	public function actionGetAllByDateDesc() : APIResponse
	{
		$blogs = Blog::getAllByDateDesc();
		$ar = new APIResponse();
		$ar->set("blogs", $blogs);

		return $ar;
	}

	public function actionCreate() : APIResponse
	{
		$rn = rand(0, 5000);
		$b = new Blog(0, "title" . $rn, "author" . $rn, time());

		$b->save();

		$ar = new APIResponse();
		$ar->set("blog", $b);

		return $ar;
	}

	public function actionPopulateWithDataFixtures() : CLIResponse
	{
		$msg = "BlogIDs: ";

		$b = new Blog(0, "", "", 0);
		$now = time();
		for ($i = 0; $i < 15; $i++) {
			// to insert
			$b->setId(0);
			$b->setTitle("title" . $i);
			$b->setAuthor("author" . $i);
			$b->setDate($now - 15 + $i);

			$b->save();

			$msg .= "\n" . $b->getId();
		}

		$cr = new CLIResponse();
		$cr->setMessage($msg);

		return $cr;
	}
}
