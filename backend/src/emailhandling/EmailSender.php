<?

namespace EmailHandling;

interface EmailSender
{
	function send(Email $e) : bool;
}
