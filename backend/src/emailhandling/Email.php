<?

namespace EmailHandling;

class Email
{
	protected $from = [];

	protected $recepients = [];

	protected $ccs = [];

	protected $bccs = [];

	protected $subject = "";

	protected $body = "";

	private function createAddressAndName(string $address, string $name) : array
	{

		return ["address" => $address, "name" => $name];
	}

	public function setFrom(string $address, string $name) : Email
	{
		$this->from = $this->createAddressAndName($address, $name);

		return $this;
	}

	public function addRecepient(string $address, string $name) : Email
	{
		$this->recepients[] = $this->createAddressAndName($address, $name);

		return $this;
	}

	public function addCC(string $address) : Email
	{
		$this->ccs[] = $address;

		return $this;
	}

	public function addBCC(string $address) : Email
	{
		$this->bccs[] = $address;

		return $this;
	}

	public function setSubject(string $subject) : Email
	{
		$this->subject = $subject;

		return $this;
	}

	public function setBody(string $body) : Email
	{
		$this->body = $body;

		return $this;
	}

	public function getFrom() : array
	{

		return $this->from;
	}

	public function getRecepients() : array
	{

		return $this->recepients;
	}

	public function getCCs() : array
	{

		return $this->ccs;
	}

	public function getBCCs() : array
	{

		return $this->bccs;
	}

	public function getSubject() : string
	{

		return $this->subject;
	}

	public function getBody() : string
	{

		return $this->body;
	}
}
