<?

namespace EmailHandling;

use kipal\fw\Component;

class FakeEmailSender extends Component implements EmailSender
{
	protected static $numberOfSentEmails = 0;

	public function send(Email $e) : bool
	{

		static::$numberOfSentEmails++;

		return true;
	}

	public static function getNumberOfSentEmails() : int
	{

		return static::$numberOfSentEmails;
	}

	public static function reset()
	{
		static::$numberOfSentEmails = 0;
	}
}
