#!/bin/bash

docker-compose -f docker-compose-test.yml up -d
docker-compose -f docker-compose-test.yml run composer run-script test
result=$?
docker-compose -f docker-compose-test.yml stop

exit $result
