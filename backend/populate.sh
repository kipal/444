#!/bin/bash

docker-compose -f docker-compose-populate.yml up -d
docker-compose -f docker-compose-populate.yml run composer run-script populate
result=$?
docker-compose -f docker-compose-populate.yml stop

exit $result
