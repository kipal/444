<?

require __DIR__ . '/vendor/autoload.php';

use kipal\fw\CLIApplication;
use kipal\fw\CLIParams;

CLIApplication::setConfig(require "./config.php");
CLIApplication::getInstance()->run(new CLIParams("blog", "populateWithDataFixtures"));
