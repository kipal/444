<?

return [
	"env" => "dev",
	"fakeError" => (bool)rand(0, 1),
	"components" => [
		"mongo" => [
			"class" => "DB\\MongoDBWrapper",
			"address" => "mongodb://mongo:27017/"
		],
		"emailClient" => [
			"class" => "EmailHandling\\FakeEmailSender"
		],
		"syslog" => [
			"class" => "ErrorReporting\\SyslogHandler",
			"ident" => "444ErrorLog",
			"option" => LOG_PID | LOG_PERROR,
			"facility" => LOG_LOCAL0,
		]
	],
	"controllerMap" => [
		"blog" => "BlogHandling\\BlogController"
	],
	"errorMail" => [
		"from" => [
			"address" => "teszt@elek.com",
			"name" => "Test Elek"
		],
		"subject" => "TestSubject",
		"recepients" => [
			["address" => "test1@developer.com", "name" => "Test Developer"]
		]
	]
];
