<?

use PHPUnit\Framework\TestCase;
use DB\MongoDBWrapper;

class MongoDBWrapperTest extends TestCase
{
	private $manager = null;

	protected function setUp()
	{
		$this->manager = new \MongoDB\Driver\Manager("mongodb://mongo:27017/");
	}

	protected function tearDown()
	{
		$bulk = new \MongoDB\Driver\BulkWrite();
		$bulk->delete([], ["limit" => false]);

		$this->manager->executeBulkWrite("db.collection", $bulk);
	}

	public function testCreate()
	{
		$w = new MongoDBWrapper();
		$w->setMembers(
			[
				"address" => "mongodb://mongo:27017/"
			]
		);
	}

	private function loadData()
	{
		$bulk = new \MongoDB\Driver\BulkWrite();
		$bulk->insert(["_id" => "test1", "testAttr" => "testValue1"]);
		$bulk->insert(
			[
				"_id" => "test2",
				"testAttr" => "testValue",
				"testNum" => 2
			]
		);
		$bulk->insert(
			[
				"_id" => "test3",
				"testAttr" => "testValue",
				"testNum" => 1
			]
		);

		$this->manager->executeBulkWrite("db.collection", $bulk);
	}

	public function testGetDocumentById()
	{
		$this->loadData();

		$w = new MongoDBWrapper();
		$w->setMembers(["address" => "mongodb://mongo:27017/"]);

		$doc = $w->getDocumentById("db.collection", "test1");
		$expected = new \stdClass;
		$expected->_id = "test1";
		$expected->testAttr = "testValue1";

		$this->assertEquals($expected, $doc);
	}

	public function testGetDocumentsByFilterAndOptions()
	{
		$this->loadData();

		$w = new MongoDBWrapper();
		$w->setMembers(["address" => "mongodb://mongo:27017/"]);

		$docs = $w->getDocumentsByFilterAndOptions("db.collection", ["testAttr" => "testValue"], [ "sort" => ["testNum" => 1]]);
		$expected1 = new \stdClass;
		$expected1->_id = "test3";
		$expected1->testAttr = "testValue";
		$expected1->testNum = 1;

		$expected2 = new \stdClass;
		$expected2->_id = "test2";
		$expected2->testAttr = "testValue";
		$expected2->testNum = 2;

		$this->assertEquals([$expected1, $expected2], $docs);
	}

	public function testInsert()
	{
		$this->loadData();

		$w = new MongoDBWrapper();
		$w->setMembers(["address" => "mongodb://mongo:27017/"]);

		$doc = new \stdClass;
		$doc->testInsertOne1 = 3;
		$id = $w->insertOneDocument("db.collection", $doc);
		$doc->_id = $id;

		$this->assertEquals($doc, $w->getDocumentById("db.collection", $id));
	}
}
