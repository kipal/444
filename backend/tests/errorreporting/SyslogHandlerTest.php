<?

use PHPUnit\Framework\TestCase;
use ErrorReporting\FakeSyslogHandler;

class SyslogHandlerTest extends TestCase
{
	public function testExample()
	{
		$sh = new FakeSyslogHandler();
		$sh->setMembers([
			"ident" => "testIdent",
			"option" => 1,
			"facility" => 2
		]);
		$sh->init();
		$this->assertTrue($sh->isOpened);
		$sh->log(3, "testMsg");

		unset($sh);
		$this->assertTrue(FakeSyslogHandler::$isClosed);
	}
}
