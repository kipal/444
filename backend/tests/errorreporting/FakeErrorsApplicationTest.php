<?

use PHPUnit\Framework\TestCase;
use ErrorReporting\FakeErrorsApplication;
use ErrorReporting\FakeSyslogHandler;
use kipal\fw\APIRequest;
use kipal\fw\APIResponse;
use EmailHandling\FakeEmailSender;

class TestController
{
	public function actionTest() : APIResponse
	{

		return new APIResponse();
	}
}

class FakeErrorsApplicationTest extends TestCase
{
	public function tearDown()
	{
		FakeErrorsApplication::removeInstance();
		FakeEmailSender::reset();
	}

	/**
	 *@expectedException ErrorReporting\FakeException
	 */
	public function testGenerateFakeError()
	{
		FakeErrorsApplication::setConfig([
			"env" => "dev",
			"fakeError" => true
		]);

		FakeErrorsApplication::getInstance()->run(new APIRequest("test", "test"));
	}

	public function testNotGenerateFakeError()
	{
		ob_start();
		FakeErrorsApplication::setConfig([
			"env" => "dev",
			"fakeError" => false
		]);

		FakeErrorsApplication::getInstance()->run(new APIRequest("test", "test"));

		$this->assertEquals("[]", ob_get_clean());
	}

	public function testErrorResponse()
	{
		ob_start();
		FakeErrorsApplication::setConfig([
			"components" => [
				"emailClient" => [
					"class" => "EmailHandling\\FakeEmailSender"
				],
				"syslog" => [
					"class" => "ErrorReporting\\FakeSyslogHandler",
					"ident" => "testIdent",
					"option" => 1,
					"facility" => 2,
				]
			],
			"env" => "dev",
			"errorMail" => [
				"from" => [
					"address" => "teszt@elek.com",
					"name" => "Test Elek"
				],
				"subject" => "TestSubject",
				"recepients" => [
					["address" => "test1@developer.com", "name" => "Test Developer"]
				]
			]
		]);

		$this->assertEquals(0, FakeEmailSender::getNumberOfSentEmails());

		FakeErrorsApplication::errorAPIResponse(new \Exception("test"));

		$this->assertEquals("{\"error\":{\"exception\":\"Exception\",\"message\":\"test\"}}", ob_get_clean());

		$this->assertEquals(1, FakeSyslogHandler::getNumberOfLogs());
		$this->assertEquals(1, FakeEmailSender::getNumberOfSentEmails());
	}
}
