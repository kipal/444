<?

use PHPUnit\Framework\TestCase;
use EmailHandling\Email;
use EmailHandling\FakeEmailSender;

class FakeEmailSenderTest extends TestCase
{
	public function testSendMail()
	{
		$es = new FakeEmailSender();

		$this->assertInstanceOf("EmailHandling\\EmailSender", $es);
		$this->assertTrue($es->send(new Email()));
	}
}
