<?

use PHPUnit\Framework\TestCase;
use EmailHandling\Email;

class EmailTest extends TestCase
{
	public function testCreate()
	{
		$e = new Email();
		$e->setFrom("test@test.com", "Test Test")
		->addRecepient("test1@test.com", "Test1 Test1")
		->addRecepient("test2@test.com", "Test2 Test2")
		->addCC("test3@test.com")
		->addCC("test4@test.com")
		->addBCC("test5@test.com")
		->addBCC("test6@test.com")
		->setSubject("testSubject")
		->setBody("testBody");

		$this->assertEquals(["address" => "test@test.com", "name" => "Test Test"], $e->getFrom());
		$this->assertEquals(
			[
				0 => ["address" => "test1@test.com", "name" => "Test1 Test1"],
				1 => ["address" => "test2@test.com", "name" => "Test2 Test2"]
			],
			$e->getRecepients()
		);
		$this->assertEquals(["test3@test.com", "test4@test.com"], $e->getCCs());
		$this->assertEquals(["test5@test.com","test6@test.com"], $e->getBCCs());
		$this->assertEquals("testSubject", $e->getSubject());
		$this->assertEquals("testBody", $e->getBody());
	}
}
