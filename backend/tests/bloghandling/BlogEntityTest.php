<?

use PHPUnit\Framework\TestCase;
use BlogHandling\Blog;
use kipal\fw\APIApplication;

class BlogEntityTest extends TestCase
{
	private $manager = null;

	private $now = 0;

	protected function setUp()
	{
		$this->manager = new \MongoDB\Driver\Manager("mongodb://mongo:27017/");
	}

	protected function tearDown()
	{
		$bulk = new \MongoDB\Driver\BulkWrite();
		$bulk->delete([], ["limit" => false]);

		$this->manager->executeBulkWrite("db.blog", $bulk);
	}

	private function getTimeStamp() : int
	{
		if (empty($this->now)) {
			$this->now = microtime(true);
		}

		return $this->now;
	}

	private function loadData()
	{
		$bulk = new \MongoDB\Driver\BulkWrite();
		$bulk->insert(
			["_id" => 1, "title" => "testTitle1", "author" => "testAuthor1", "date" => $this->getTimeStamp()]
		);
		$bulk->insert(
			["_id" => 2, "title" => "testTitle2", "author" => "testAuthor2", "date" => $this->getTimeStamp() + 1]
		);
		$bulk->insert(
			["_id" => 3, "title" => "testTitle3", "author" => "testAuthor3", "date" => $this->getTimeStamp() + 2]
		);

		$this->manager->executeBulkWrite("db.blog", $bulk);

		APIApplication::setConfig(
			[
				"env" => "dev",
				"components" => [
					"mongo" => [
						"class" => "DB\\MongoDBWrapper",
						"address" => "mongodb://mongo:27017"
					]
				]
			]
		);
	}

	public function testGetAnEntityById()
	{
		$this->loadData();

		$b = Blog::getById(1);

		$this->assertEquals(new Blog(1, "testTitle1", "testAuthor1", $this->getTimeStamp()), $b);
	}

	public function testDocumentsByDateSort()
	{
		$this->loadData();

		$blogs = Blog::getAllByDateDesc();


		$this->assertEquals(
			[
				new Blog(3, "testTitle3", "testAuthor3", $this->getTimeStamp() + 2),
				new Blog(2, "testTitle2", "testAuthor2", $this->getTimeStamp() + 1),
				new Blog(1, "testTitle1", "testAuthor1", $this->gettimestamp())
			],
			$blogs
		);
	}

	public function testSaveBlog()
	{
		$this->loadData();

		$b = new Blog(0, "testTitle0", "testAuthor0", $this->getTimeStamp());
		$id = $b->save();

		$this->assertEquals($b, Blog::getById($id));
	}

	public function testJSONSerialize()
	{
		$b = new Blog(1, "testTitle1", "testAuthor1", $this->getTimeStamp());
		$date = new \DateTime("@" . $this->getTimeStamp());
		$this->assertEquals(
			json_encode(
				[
					"id"=> 1,
					"title"=> "testTitle1",
					"author"=> "testAuthor1",
					"date"=> $date->format("D M d Y H:i:s O")
				]
			),
			json_encode($b)
		);
	}
}
