Third-party kódot csak fejlesztéshez használtam (phpunit).
Linuxon fejlesztettem, irtam pár shell scriptet, hogy egyszerűbb legyen az élet, viszont ezeket nem teszteltem Mac-en.

Amikre nem maradt idő:
- a mock-kolást próbáltam a legegyszerűbben megoldani, hogy ne menjen el vele az idő, igy azokon lehetne még bőven húzni
- kódformázás (de szerintem ez csak sokadlagos kérdés, toolokkal megoldaható egyszerűen
- kommentek
- részletes dokumentáció
- valamint a fejlesztett framework kifejezetten ezekhez a funkciókhoz készült, szóval meglehetősen minimális


### Install

```
git clone https://kipal@bitbucket.org/kipal/444.git
cd 444
./install.sh
```

### Usage

```
cd 444
docker-compose up
```
