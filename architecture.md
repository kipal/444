I tried to keep the architecture simple.
Frontend container built on the nginx image, backend container on the php-fpm image. I could have use a 'web' container which has nginx and php-fpm in it but this way they can be scaled by applications vertically if it is necessary.
Consequently, the frontend containers has to reach the backend container through links.

Nginx config is simple too. For the / location the request is handled by the nginx webserver which gives the index.html as a response.
For the /api location the request is going to be sent to the backend for handling it.

